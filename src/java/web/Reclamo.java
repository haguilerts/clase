package web;

import com.google.gson.Gson;
import entidades.Reclamos;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Reclamo", urlPatterns = {"/Reclamo"})
public class Reclamo extends HttpServlet {

    Gson convertir = new Gson();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Estamos en el metodo POST");

        Reclamos miReclamo = new Reclamos();
        miReclamo.email = "pepe@gamilc.om";
        miReclamo.nombre = "Pepe";
        miReclamo.texto = "Testo del reclamo";

        System.out.println(miReclamo);
        
        Reclamos miReclamo2 = convertir.fromJson(req.getReader().readLine(), Reclamos.class);
        
        System.out.println("Reclamo en java: " + miReclamo2);
        System.out.println("Mail del reclamante: " + miReclamo2.email);

        System.out.println();
        resp.getWriter().print("Recibimos su pedido, aguarde sentado.");
    }

}
